# CHANGELOG
**v5.5**<br>
*fix*       : Migrating Pwar's database would not work because the SnDdb database was not created yet. Fixed.<br>
*fix*       : Quick kill commands would not accept multiple word commands.<br>
*feature*   : 'xhelp summary' will provide a summary of all commands. For more details, visit each help file.<br>

**v5.4**<br>
*fix*       : The quick kill command was not executing single letter commands. Now fixed.<br>
**v5.3**<br>
*fix*       : Fixed (hopefully) instances where the mob is killed in one hit and not added to mob database.<br>

**v5.2**<br>
*fix*       : Fixed all the previous bugs relating to indexing errors.<br>
*fix*       : Added 'ak' back to the public version and fixed the bug that would split commands erroneously.<br>

**v5.1**<br>
*fix*       : Attempted to fix, failed misrable. See 5.2.<br>

**v5.0**<br>
*feature*   : Added long-awaited mob database!<br>
*feature*   : Added 'snd reload' so you do not have to open Plugins to reinstall.<br>
*change*    : Changed how the help files look. See 'xhelp' for more info.<br>
*Notes*     : Still working on a fix for a couple bugs, but wanted to push this out in the meantime.<br>

**v4.60**<br>
*feature*   : Added changelog per request.<br>
*update*    : Updated the Readme.<br>
*change*    : Changed "Send" to "Execute" on kill command. If there are any problems then blame it on someone else!<br>

**Previous versions**<br>
*update*    : Not going to go through all the updates made. Will update this log going forward.